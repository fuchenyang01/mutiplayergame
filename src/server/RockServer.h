#include "Rock.h"
#include "NetworkManagerServer.h"

class RockServer : public Rock
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new RockServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithPlayer(Player* inCat) override;

protected:
	RockServer();

};