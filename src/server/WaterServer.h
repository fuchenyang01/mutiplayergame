#include "Water.h"
#include "NetworkManagerServer.h"

class WaterServer : public Water
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new WaterServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithPlayer(Player* inCat) override;

protected:
	WaterServer();

};