#include"BulletServer.h"

#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"
#include"Player.h"
#include"PlayerServer.h"

BulletServer::BulletServer()
{
	//yarn lives a second...
	mTimeToDie = Timing::sInstance.GetFrameStartTime() + 3.f;
}

void BulletServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}


void BulletServer::Update()
{
	Bullet::Update();

	if (Timing::sInstance.GetFrameStartTime() > mTimeToDie)
	{
		SetDoesWantToDie(true);
	}

}

bool BulletServer::HandleCollisionWithPlayer(Player* inCat)
{
	if (inCat->GetPlayerId() != GetPlayerId())
	{
		//kill yourself!
		SetDoesWantToDie(true);

		static_cast<PlayerServer*>(inCat)->TakeDamage(GetPlayerId());

	}

	return false;
}
