#include "Wall.h"
#include "NetworkManagerServer.h"

class WallServer : public Wall
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new WallServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithPlayer(Player* inCat) override;

protected:
	WallServer();

};