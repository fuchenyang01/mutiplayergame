#include "Grass.h"
#include "NetworkManagerServer.h"

class GrassServer : public Grass
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new GrassServer()); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithPlayer(Player* inCat) override;

protected:
	GrassServer();

};