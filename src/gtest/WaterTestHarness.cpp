#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "WaterTestHarness.h"
#include "Water.h"
#include "WaterClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

WaterTestHarness::WaterTestHarness()
{
	Wp = nullptr;
}

WaterTestHarness::~WaterTestHarness()
{
	Wp.reset();
}

void WaterTestHarness::SetUp()
{
    GameObject*	go = Water::StaticCreate();
	Water* p = static_cast<Water*>(go);
    this->Wp.reset(p);
}

void WaterTestHarness::TearDown()
{
    this->Wp.reset();
    this->Wp = nullptr;
}

TEST_F(WaterTestHarness,constructor_noArgs)
{
  // Check defaults are set
  // Should be no need to do these as they were tested with the base class.
  EXPECT_TRUE(Maths::Is3DVectorEqual(Wp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(Wp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(Wp->GetCollisionRadius(), 0.25f);
  EXPECT_FLOAT_EQ(Wp->GetScale(),1.5f);
  EXPECT_FLOAT_EQ(Wp->GetRotation(),0.0f);
  EXPECT_EQ(Wp->GetIndexInWorld(), -1);
  EXPECT_EQ(Wp->GetNetworkId(), 0);



  //Initial state is update all
  int check = 0x0003; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(Wp->GetAllStateMask(), check);

  //Check our macro has worked.
  EXPECT_EQ(Wp->GetClassId(), 'ROCK');
  EXPECT_NE(Wp->GetClassId(), 'HELP');

  //Added some getters so I could check these - not an easy class to test.

}


/* Tests Omitted
* There's a good chunk of this which cannot be tested in this limited example,
* however there should be enough to underake some testing of the serialisation code.
*/

TEST_F(WaterTestHarness,EqualsOperator1)
{ /* Won't compile - why?
  Player a ();
  Player b ();

  a.SetPlayerId(10);
  b.SetPlayerId(10);

  EXPECT_TRUE(a == b);*/
}

TEST_F(WaterTestHarness,EqualsOperator2)
{
	Water*a = static_cast<Water*>(Water::StaticCreate());
	Water*b = static_cast<Water*>(Water::StaticCreate());



  EXPECT_TRUE(*a == *b);
}




/* No longer works, as this has been moved to the PlayerClient class */
TEST_F(WaterTestHarness,serialiseAndDeserialisePlayer)
{ 
  std::shared_ptr<OutputMemoryBitStream> out;
  std::shared_ptr<InputMemoryBitStream> in;

  const int BUFF_MAX = 512;
  char* bigBuffer = new char[BUFF_MAX]; //testing only - gets replaced.
  in.reset(new InputMemoryBitStream(bigBuffer,BUFF_MAX));
  out.reset(new OutputMemoryBitStream());

  WaterPtr readIn(static_cast<Water*>(Water::StaticCreate()));

   EXPECT_TRUE(*Wp == *readIn); //expect constructed objs to be the same.

  // change this one a bit so I know the changes have copied over.
   //Rp->SetPlayerId(20);

  //OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState
  uint32_t state = 0x000F;

  EXPECT_FALSE(*Wp == *readIn); //with one changed should be different.

  //write it into a buffer.
  Wp->Write(*out,state);

  // ... imagine networking goes on and we get an
  // actually we're connecting the output buffer to the input.
  // copy the buffer first (or we get double de-allocation)
  int copyLen = out->GetByteLength();
  char* copyBuff = new char[copyLen];
  memcpy(copyBuff, out->GetBufferPtr(), copyLen);

  in.reset(new InputMemoryBitStream(copyBuff, copyLen));

  // update from our server.
  readIn->Read(*in);

  // expect these to now be the same.
  EXPECT_TRUE(*Wp == *readIn); //expect constructed objs to be the same. 
  
}
