#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "BulletTestHarness.h"
#include "Bullet.h"
#include "BulletClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

BulletTestHarness::BulletTestHarness()
{
  bp = nullptr;
}

BulletTestHarness::~BulletTestHarness()
{
	bp.reset();
}

void BulletTestHarness::SetUp()
{
    GameObject*	go = Bullet::StaticCreate();
    Bullet* b = static_cast<Bullet*>(go);
    this->bp.reset(b);
}

void BulletTestHarness::TearDown()
{
    this->bp.reset();
    this->bp = nullptr;
}

TEST_F(BulletTestHarness,constructor_noArgs)
{
  // Check defaults are set
  // Should be no need to do these as they were tested with the base class.
  EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetColor(), Colors::White));
  EXPECT_FALSE(Maths::Is3DVectorEqual(bp->GetColor(), Colors::Black));
  EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetLocation(), Vector3::Zero));
  ASSERT_FALSE(Maths::Is3DVectorEqual(bp->GetLocation(), Vector3::UnitX));
  EXPECT_FLOAT_EQ(bp->GetCollisionRadius(), 0.01f);
  EXPECT_NE(bp->GetCollisionRadius(), 0.1f);
  EXPECT_FLOAT_EQ(bp->GetScale(),1.f);
  EXPECT_NE(bp->GetScale(), 2.f);
  EXPECT_FLOAT_EQ(bp->GetRotation(),0.0f);
  EXPECT_NE(bp->GetRotation(), 1.0f);
  EXPECT_EQ(bp->GetIndexInWorld(), -1);
  EXPECT_NE(bp->GetIndexInWorld(), 1);
  EXPECT_EQ(bp->GetNetworkId(), 0);
  EXPECT_NE(bp->GetNetworkId(), 1);

  EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetVelocity(), Vector3::Zero));
  EXPECT_EQ(bp->GetPlayerId(), 0.0f);
  EXPECT_NE(bp->GetPlayerId(), 1.0f);

  //Initial state is update all
  int check = 0x0007; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(bp->GetAllStateMask(), check);
  EXPECT_NE(bp->GetAllStateMask(), 0x000F);

  //Check our macro has worked.
  EXPECT_EQ(bp->GetClassId(), 'BULT');
  EXPECT_NE(bp->GetClassId(), 'HELP');

  //Added some getters so I could check these - not an easy class to test.
  
  EXPECT_FLOAT_EQ(bp->GetSpeed(), 3.f);
  EXPECT_NE(bp->GetSpeed(), 4.f);

}


/* Tests Omitted
* There's a good chunk of this which cannot be tested in this limited example,
* however there should be enough to underake some testing of the serialisation code.
*/

TEST_F(BulletTestHarness,EqualsOperator1)
{ /* Won't compile - why?
  Player a ();
  Player b ();

  a.SetPlayerId(10);
  b.SetPlayerId(10);

  EXPECT_TRUE(a == b);*/
}

TEST_F(BulletTestHarness,EqualsOperator2)
{
  Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
  Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

  a->SetPlayerId(10);
  b->SetPlayerId(10);

  EXPECT_TRUE(*a == *b);
}

/* Need more tests here */

TEST_F(BulletTestHarness,EqualsOperator3)
{
	Bullet *a = static_cast<Bullet*>(Bullet::StaticCreate());
	Bullet *b = static_cast<Bullet*>(Bullet::StaticCreate());

  a->SetPlayerId(10);
  b->SetPlayerId(30);

  EXPECT_FALSE(*a == *b);
}

TEST_F(BulletTestHarness,EqualsOperator4)
{
	BulletPtr b(static_cast<Bullet*>(Bullet::StaticCreate()));

  bp->SetPlayerId(10);
  b->SetPlayerId(10);

  EXPECT_TRUE(*bp == *b);
}
/* No longer works, as this has been moved to the PlayerClient class */
TEST_F(BulletTestHarness,serialiseAndDeserialisePlayer)
{ 
  std::shared_ptr<OutputMemoryBitStream> out;
  std::shared_ptr<InputMemoryBitStream> in;

  const int BUFF_MAX = 512;
  char* bigBuffer = new char[BUFF_MAX]; //testing only - gets replaced.
  in.reset(new InputMemoryBitStream(bigBuffer,BUFF_MAX));
  out.reset(new OutputMemoryBitStream());

  BulletPtr readIn(static_cast<Bullet*>(Bullet::StaticCreate()));

   EXPECT_TRUE(*bp == *readIn); //expect constructed objs to be the same.

  // change this one a bit so I know the changes have copied over.
  bp->SetPlayerId(20);

  //OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState
  uint32_t state = 0x000F;

  EXPECT_FALSE(*bp == *readIn); //with one changed should be different.

  //write it into a buffer.
  bp->Write(*out,state);

  // ... imagine networking goes on and we get an
  // actually we're connecting the output buffer to the input.
  // copy the buffer first (or we get double de-allocation)
  int copyLen = out->GetByteLength();
  char* copyBuff = new char[copyLen];
  memcpy(copyBuff, out->GetBufferPtr(), copyLen);

  in.reset(new InputMemoryBitStream(copyBuff, copyLen));

  // update from our server.
  readIn->Read(*in);

  // expect these to now be the same.
  EXPECT_TRUE(*bp == *readIn); //expect constructed objs to be the same. 
  
}
