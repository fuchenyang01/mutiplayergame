#ifndef WALL_TESTHARNESS_H_
#define WALL_TESTHARNESS_H_

#include <limits.h>
#include <gtest/gtest.h>

#include "Wall.h"

class WallTestHarness : public ::testing::Test
{
protected:

  virtual void SetUp();
  virtual void TearDown();

  WallPtr Wp;

public:

	WallTestHarness();
    virtual ~WallTestHarness();
};

#endif // WALL_TESTHARNESS_H_
