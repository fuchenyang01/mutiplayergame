#ifndef GRASS_TESTHARNESS_H_
#define GRASS_TESTHARNESS_H_

#include <limits.h>
#include <gtest/gtest.h>

#include "Grass.h"

class GrassTestHarness : public ::testing::Test
{
protected:

  virtual void SetUp();
  virtual void TearDown();

  GrassPtr Gp;

public:

	GrassTestHarness();
    virtual ~GrassTestHarness();
};

#endif // GRASS_TESTHARNESS_H_
