#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "GrassTestHarness.h"
#include "Grass.h"
#include "GrassClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

GrassTestHarness::GrassTestHarness()
{
	Gp = nullptr;
}

GrassTestHarness::~GrassTestHarness()
{
	Gp.reset();
}

void GrassTestHarness::SetUp()
{
    GameObject*	go = Grass::StaticCreate();
	Grass* p = static_cast<Grass*>(go);
    this->Gp.reset(p);
}

void GrassTestHarness::TearDown()
{
    this->Gp.reset();
    this->Gp = nullptr;
}

TEST_F(GrassTestHarness,constructor_noArgs)
{
  // Check defaults are set
  // Should be no need to do these as they were tested with the base class.
  EXPECT_TRUE(Maths::Is3DVectorEqual(Gp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(Gp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(Gp->GetCollisionRadius(), 0.25f);
  EXPECT_FLOAT_EQ(Gp->GetScale(),1.5f);
  EXPECT_FLOAT_EQ(Gp->GetRotation(),0.0f);
  EXPECT_EQ(Gp->GetIndexInWorld(), -1);
  EXPECT_EQ(Gp->GetNetworkId(), 0);



  //Initial state is update all
  int check = 0x0003; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(Gp->GetAllStateMask(), check);

  //Check our macro has worked.
  EXPECT_EQ(Gp->GetClassId(), 'GRAS');
  EXPECT_NE(Gp->GetClassId(), 'HELP');

  //Added some getters so I could check these - not an easy class to test.

}


/* Tests Omitted
* There's a good chunk of this which cannot be tested in this limited example,
* however there should be enough to underake some testing of the serialisation code.
*/

TEST_F(GrassTestHarness,EqualsOperator1)
{ /* Won't compile - why?
  Player a ();
  Player b ();

  a.SetPlayerId(10);
  b.SetPlayerId(10);

  EXPECT_TRUE(a == b);*/
}

TEST_F(GrassTestHarness,EqualsOperator2)
{
	Grass*a = static_cast<Grass*>(Grass::StaticCreate());
	Grass*b = static_cast<Grass*>(Grass::StaticCreate());



  EXPECT_TRUE(*a == *b);
}




/* No longer works, as this has been moved to the PlayerClient class */
TEST_F(GrassTestHarness,serialiseAndDeserialisePlayer)
{ 
  std::shared_ptr<OutputMemoryBitStream> out;
  std::shared_ptr<InputMemoryBitStream> in;

  const int BUFF_MAX = 512;
  char* bigBuffer = new char[BUFF_MAX]; //testing only - gets replaced.
  in.reset(new InputMemoryBitStream(bigBuffer,BUFF_MAX));
  out.reset(new OutputMemoryBitStream());

  GrassPtr readIn(static_cast<Grass*>(Grass::StaticCreate()));

   EXPECT_TRUE(*Gp == *readIn); //expect constructed objs to be the same.

  // change this one a bit so I know the changes have copied over.
   //Rp->SetPlayerId(20);

  //OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState
  uint32_t state = 0x000F;

  EXPECT_FALSE(*Gp == *readIn); //with one changed should be different.

  //write it into a buffer.
  Gp->Write(*out,state);

  // ... imagine networking goes on and we get an
  // actually we're connecting the output buffer to the input.
  // copy the buffer first (or we get double de-allocation)
  int copyLen = out->GetByteLength();
  char* copyBuff = new char[copyLen];
  memcpy(copyBuff, out->GetBufferPtr(), copyLen);

  in.reset(new InputMemoryBitStream(copyBuff, copyLen));

  // update from our server.
  readIn->Read(*in);

  // expect these to now be the same.
  EXPECT_TRUE(*Gp == *readIn); //expect constructed objs to be the same. 
  
}
