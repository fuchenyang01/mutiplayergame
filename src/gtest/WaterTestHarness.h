#ifndef WATER_TESTHARNESS_H_
#define WATER_TESTHARNESS_H_

#include <limits.h>
#include <gtest/gtest.h>

#include "Water.h"

class WaterTestHarness : public ::testing::Test
{
protected:

  virtual void SetUp();
  virtual void TearDown();

  WaterPtr Wp;

public:

	WaterTestHarness();
    virtual ~WaterTestHarness();
};

#endif // WATER_TESTHARNESS_H_
