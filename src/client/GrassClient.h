#include "SpriteComponent.h"
#include"Grass.h"
class GrassClient : public Grass
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new GrassClient()); }

protected:
	GrassClient();

private:

	SpriteComponentPtr	mSpriteComponent;
};