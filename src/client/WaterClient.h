#include "SpriteComponent.h"
#include"Water.h"
class WaterClient : public Water
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new WaterClient()); }

protected:
	WaterClient();

private:

	SpriteComponentPtr	mSpriteComponent;
};