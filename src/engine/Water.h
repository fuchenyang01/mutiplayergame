#ifndef WATER_H_
#define WATER_H_

#include "GameObject.h"
#include "World.h"
class Water : public GameObject
{
public:
	CLASS_IDENTIFICATION('WATR', GameObject)

		enum ERockReplicationState
	{
		EMRS_Pose = 1 << 0,
		EMRS_Color = 1 << 1,

		EMRS_AllState = EMRS_Pose | EMRS_Color
	};

	static	GameObject* StaticCreate() { return new Water(); }

	virtual uint32_t	GetAllStateMask()	const override { return EMRS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void		Read(InputMemoryBitStream& inInputStream) override;

	virtual bool HandleCollisionWithPlayer(Player* Player) override;

protected:
	Water();

};
typedef shared_ptr< Water >	WaterPtr;
#endif // ROCK_H_