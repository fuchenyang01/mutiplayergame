var struct_replication_command =
[
    [ "ReplicationCommand", "struct_replication_command.html#af4ae283e48a92afbf9be6cd8d5c41fcc", null ],
    [ "ReplicationCommand", "struct_replication_command.html#ae86c6abc79a8c42a52e2d7c55fd3ef89", null ],
    [ "AddDirtyState", "struct_replication_command.html#a3b5ce18c2a24f584c9e990d128570791", null ],
    [ "ClearDirtyState", "struct_replication_command.html#ab9d071654827f4395506fd1be7e02361", null ],
    [ "GetAction", "struct_replication_command.html#a4755cfcb6c242527c6fd04e234238e26", null ],
    [ "GetDirtyState", "struct_replication_command.html#aa7de85db4d2ef4a4479deb508d16da70", null ],
    [ "HandleCreateAckd", "struct_replication_command.html#a433ee10635ddd5b81a6b5dfbc8e94ae3", null ],
    [ "HasDirtyState", "struct_replication_command.html#a1ad99de8118a953621e83eb88b6603b4", null ],
    [ "SetAction", "struct_replication_command.html#ae03cf309f0172d0f39daf3c4f0d576c5", null ],
    [ "SetDestroy", "struct_replication_command.html#a34e62dd19656d7ede5f0d03f0868effd", null ]
];