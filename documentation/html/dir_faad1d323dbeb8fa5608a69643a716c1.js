var dir_faad1d323dbeb8fa5608a69643a716c1 =
[
    [ "BitMan.h", "_bit_man_8h.html", [
      [ "GetRequiredBitsHelper", "struct_get_required_bits_helper.html", "struct_get_required_bits_helper" ],
      [ "GetRequiredBitsHelper< 0, tBits >", "struct_get_required_bits_helper_3_010_00_01t_bits_01_4.html", "struct_get_required_bits_helper_3_010_00_01t_bits_01_4" ],
      [ "GetRequiredBits", "struct_get_required_bits.html", "struct_get_required_bits" ]
    ] ],
    [ "Colors.h", "_colors_8h.html", null ],
    [ "Maths.cpp", "_maths_8cpp.html", null ],
    [ "Maths.h", "_maths_8h.html", "_maths_8h" ],
    [ "Quaternion.h", "_quaternion_8h.html", [
      [ "Quaternion", "class_quaternion.html", "class_quaternion" ]
    ] ],
    [ "RandGen.cpp", "_rand_gen_8cpp.html", null ],
    [ "RandGen.h", "_rand_gen_8h.html", [
      [ "RandGen", "class_rand_gen.html", "class_rand_gen" ]
    ] ],
    [ "Vector3.cpp", "_vector3_8cpp.html", null ],
    [ "Vector3.h", "_vector3_8h.html", [
      [ "Vector3", "class_vector3.html", "class_vector3" ]
    ] ]
];