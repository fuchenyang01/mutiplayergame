var class_test_object =
[
    [ "TestObject", "class_test_object.html#a3d0036f18982129567596b06d72087b6", null ],
    [ "TestObject", "class_test_object.html#a87d4cf82f2de377b29d8c06f40103729", null ],
    [ "~TestObject", "class_test_object.html#a3262a3065422e5ebd007e38c4c50d01f", null ],
    [ "getA", "class_test_object.html#af0c626f991a989a15cb880c96391ec8a", null ],
    [ "getB", "class_test_object.html#ad2de4d7d751cddffd6fe46837897da86", null ],
    [ "getName", "class_test_object.html#adce1e7879612852c5ba5e19d5651bb6e", null ],
    [ "operator==", "class_test_object.html#ad8cb6f58d4b315ea6013d07e32b91b03", null ],
    [ "Read", "class_test_object.html#aae7f787ad2b9250058f3b981c0575018", null ],
    [ "setA", "class_test_object.html#abd94981908e251344262daba184789e9", null ],
    [ "setB", "class_test_object.html#a5ebd6f82898dafa4b6bc3039fdf66550", null ],
    [ "setName", "class_test_object.html#a3cff9cce5c5aea3482c760fdb22afab2", null ],
    [ "Write", "class_test_object.html#abc171c00b49b7ddca3641aa72af2e4fe", null ]
];