var class_score_board_manager =
[
    [ "Entry", "class_score_board_manager_1_1_entry.html", "class_score_board_manager_1_1_entry" ],
    [ "AddEntry", "class_score_board_manager.html#a8647decdf12c730851fe14e21fccb62d", null ],
    [ "GetEntries", "class_score_board_manager.html#a6d32074f30107c7f7cb39b625f6c9b44", null ],
    [ "GetEntry", "class_score_board_manager.html#ac9c3ac6e2c49f55c02e82e7af9f36450", null ],
    [ "IncScore", "class_score_board_manager.html#af6b326d8f50cbb2cc8131803ac159912", null ],
    [ "Read", "class_score_board_manager.html#af8265c5da67fc27de66909376ae43513", null ],
    [ "RemoveEntry", "class_score_board_manager.html#a9abd4123effc9a69b0f1053f95cafb53", null ],
    [ "Write", "class_score_board_manager.html#a5df4a5b0276c231b7adb95f93c52d06e", null ]
];