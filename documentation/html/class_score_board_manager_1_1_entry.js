var class_score_board_manager_1_1_entry =
[
    [ "Entry", "class_score_board_manager_1_1_entry.html#a2c6bf734bf3678ea56269ddf5415ab17", null ],
    [ "Entry", "class_score_board_manager_1_1_entry.html#a397c4b3547e122d3d517cbce27abdb2c", null ],
    [ "GetColor", "class_score_board_manager_1_1_entry.html#a432c52c0d8a507f6d0f38caa69cba0ac", null ],
    [ "GetFormattedNameScore", "class_score_board_manager_1_1_entry.html#a49d76a11bee401a60dc536e0203181ab", null ],
    [ "GetPlayerId", "class_score_board_manager_1_1_entry.html#a91ebecfbf6f51dd37e8e51d3d5e7ef1f", null ],
    [ "GetPlayerName", "class_score_board_manager_1_1_entry.html#aee4f8a24c690e8dc38f6bbf2618079e8", null ],
    [ "GetScore", "class_score_board_manager_1_1_entry.html#a6de869e46c597490e88597d648cf58c8", null ],
    [ "Read", "class_score_board_manager_1_1_entry.html#a38d570d5cc9ae89f2bede21693c780d6", null ],
    [ "SetScore", "class_score_board_manager_1_1_entry.html#afca05810c27a2e3bc1914041090cb23c", null ],
    [ "Write", "class_score_board_manager_1_1_entry.html#a7d0bf9fc72bcea114c97fef9ae45974b", null ]
];