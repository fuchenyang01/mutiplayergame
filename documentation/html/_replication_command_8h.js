var _replication_command_8h =
[
    [ "ReplicationCommand", "struct_replication_command.html", "struct_replication_command" ],
    [ "ReplicationAction", "_replication_command_8h.html#a55d2cc3a0046e5639c1b06f02a5f6b08", [
      [ "RA_Create", "_replication_command_8h.html#a55d2cc3a0046e5639c1b06f02a5f6b08a13965cf96c61f34a94fda37dca5058bf", null ],
      [ "RA_Update", "_replication_command_8h.html#a55d2cc3a0046e5639c1b06f02a5f6b08a2f60a762c9392dad6592eb79674eae2d", null ],
      [ "RA_Destroy", "_replication_command_8h.html#a55d2cc3a0046e5639c1b06f02a5f6b08a6b7d28e20d9684b623b65347e3ac7c4f", null ],
      [ "RA_RPC", "_replication_command_8h.html#a55d2cc3a0046e5639c1b06f02a5f6b08aee045bebed03e7548521aec178696e5a", null ],
      [ "RA_MAX", "_replication_command_8h.html#a55d2cc3a0046e5639c1b06f02a5f6b08a9477d45119a0837e5614aafd62c040b2", null ]
    ] ]
];