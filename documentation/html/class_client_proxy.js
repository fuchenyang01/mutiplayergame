var class_client_proxy =
[
    [ "ClientProxy", "class_client_proxy.html#a98c9bb161ba94aefd08ee5ad0fef3077", null ],
    [ "GetLastPacketFromClientTime", "class_client_proxy.html#a54d8e637eb4704336c9d324e9481a7d0", null ],
    [ "GetName", "class_client_proxy.html#a4dc471e2aeb7a1645e7a1e64270b9d68", null ],
    [ "GetPlayerId", "class_client_proxy.html#a2b48cf6915300eab991f904d4bec9426", null ],
    [ "GetReplicationManagerServer", "class_client_proxy.html#a2a8f7e62e9146d61c1b76ddbaf082e86", null ],
    [ "GetSocketAddress", "class_client_proxy.html#aa8ee3fe46704bc1bc302fbb9d7584bf9", null ],
    [ "GetUnprocessedMoveList", "class_client_proxy.html#afe3554eee6953070419fb0a1d260e8d9", null ],
    [ "GetUnprocessedMoveList", "class_client_proxy.html#a672ce935ed1b43df0e382b523c61b2e6", null ],
    [ "HandlePlayerDied", "class_client_proxy.html#aa729acf6d1ad5d6020aa13e945ca2625", null ],
    [ "IsLastMoveTimestampDirty", "class_client_proxy.html#a174b42bcd5140b854765da6ba6865a5f", null ],
    [ "RespawnPlayerIfNecessary", "class_client_proxy.html#a1ac59e407f76901f4b72f4ab40bce0d7", null ],
    [ "SetIsLastMoveTimestampDirty", "class_client_proxy.html#a97bea60e1fb50fc366697d21ea9247d8", null ],
    [ "UpdateLastPacketTime", "class_client_proxy.html#a2f8cae26a112c87e7890ad02d346768a", null ]
];